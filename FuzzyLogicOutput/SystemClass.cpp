#include "SystemClass.hpp"

SystemClass::SystemClass(void) :
	m_ProgramRunning(true),
	m_DistanceInput(0),
	m_AccelerationInput(0),
	m_FuzzyOutput(0),
	m_FuzzyHandler(NULL),
	m_DistanceSet(NULL),
	m_AccelerationSet(NULL),
	m_DefuzzificationSet(NULL),
	m_FuzzyRules(NULL),
	m_NumberOfFuzzyRules(0),
	m_DefuzzifictionValues(NULL),
	m_DefuzzifictionResult(0)
{
}

SystemClass::~SystemClass(void)
{
}

void SystemClass::Initialise(void)
{
	// Display beginning text
	std::cout << "This is the testing system for the Driving Fuzzy Inference System." << std::endl << std::endl;
	std::cout << "Two inputs are required, the distance of the car from the driving line and the rate of change of the distance from the driving line." << std::endl << std::endl;
	std::cout << "The input range for the distance is between -100 to 100 units, representing being far left or far right of the driving line." << std::endl << std::endl;
	std::cout << "The input range for the rate of change of the distance is between -100 to 100 units, representing moving quickly left or quickly right." << std::endl << std::endl;
	std::cout << "Input values outside the term ranges are valid and are represented as 100% or -100% depending on being left or right." << std::endl << std::endl;
	std::cout << "The output is a percentage value of how much the steering wheel of the car needs to turn, -100% indicates turning full lock left, and 100% represents turning full lock right." << std::endl << std::endl;

	std::cout << std::endl << "Press any Key to continue.";

	_getch();

	ClearScreen();

	CreateFuzzySets();
	CreateFuzzyRules();
}
void SystemClass::Run(void)
{
	while(m_ProgramRunning) {
		// Create local stores
		double *l_DistanceMembership = NULL;
		double *l_AccelerationMembership = NULL;
		double *l_CurrentRuleArguments = NULL;
		double l_RuleResult = 0;
		int l_CurrentRule = 0;

		// Get Input
		m_DistanceInput = GetDistanceInput();
		m_AccelerationInput = GetAccelerationInput();

		// Pass input to fuzzy system
		l_DistanceMembership = m_FuzzyHandler->Run(m_DistanceSet, m_DistanceInput);
		l_AccelerationMembership = m_FuzzyHandler->Run(m_AccelerationSet, m_AccelerationInput);

		std::cout << "\nThe membership values for the distance set are: " << std::endl;
		for(int i = 0; i < m_DistanceSet->GetNumberOfFuzzyValues(); i++) {
			std::cout << l_DistanceMembership[i] << "\t" << m_DistanceSet->GetFuzzyValuesName(i) << std::endl;		
		}

		std::cout << "\nThe membership values for the acceleration set are: " << std::endl;
		for(int i = 0; i < m_AccelerationSet->GetNumberOfFuzzyValues(); i++) {
			std::cout << l_AccelerationMembership[i] << "\t" << m_AccelerationSet->GetFuzzyValuesName(i) << std::endl;		
		}
		
		l_CurrentRuleArguments = new double[2];
		m_DefuzzifictionValues = new double[m_DefuzzificationSet->GetNumberOfFuzzyValues()];

		for(int i = 0; i < m_DefuzzificationSet->GetNumberOfFuzzyValues(); i++) {
			m_DefuzzifictionValues[i] = 0;
		}

		for(int i = 0; i < m_DistanceSet->GetNumberOfFuzzyValues(); i++) {
			for(int j = 0; j < m_AccelerationSet->GetNumberOfFuzzyValues(); j++) {
				l_CurrentRuleArguments[0] = l_DistanceMembership[i];
				l_CurrentRuleArguments[1] = l_AccelerationMembership[j]; 

				l_CurrentRule = (i*m_DistanceSet->GetNumberOfFuzzyValues()) + j;

				l_RuleResult = m_FuzzyRules[l_CurrentRule].RunRule(l_CurrentRuleArguments);
				AddToResults((i+j), l_RuleResult);
			}
		}

		std::cout << std::endl << "The membership values for the defuzzification set are: " << std::endl;
		for(int i = 0; i < m_DefuzzificationSet->GetNumberOfFuzzyValues(); i++) {
			if(m_DefuzzifictionValues[i] > 1.0) {
				m_DefuzzifictionValues[i] = 1.0;
			}
			std::cout << m_DefuzzifictionValues[i] << "\t" << m_DefuzzificationSet->GetFuzzyValuesName(i) << std::endl;
		}
		
		m_DefuzzifictionResult  = m_FuzzyHandler->Defuzzification(m_DefuzzifictionValues, m_DefuzzificationSet->GetNumberOfFuzzyValues(), m_DefuzzificationSet);

		// To ensure the number of decimal points are not silly, the result is 
		// multiplied by 1000, cast as an int,  then devided by 1000 
		m_DefuzzifictionResult = m_DefuzzifictionResult*1000;
		m_DefuzzifictionResult = (int)m_DefuzzifictionResult;
		m_DefuzzifictionResult = m_DefuzzifictionResult/1000;

		// Get the reverse to see which direction to turn
		m_DefuzzifictionResult = -m_DefuzzifictionResult;

		std::cout << std::endl << "The final output of the system is:" << std::endl << m_DefuzzifictionResult << "%" << std::endl << std::endl << std::endl;

		std::cout << "Press 'X' to exit or a'C' to continue" << "\t";

		std::string input;
		std::cin >> input;

		while(true) {
			if(input.compare("x") == 0) {
				m_ProgramRunning = false;
				break;
			}
			else if(input.compare("c") == 0) {
				break;
			}
			else {
				std::cout << "\nInvalid command. Try again: ";
				std::cin.clear();
				std::cin.ignore(256, '\n');
				std::cin >> input;
			}
		}

		std::cout << std::endl << std::endl;

		ClearScreen();
	}

	return;
}

int SystemClass::Shutdown(void)
{
	return 0;
}

double SystemClass::GetDistanceInput(void)
{
	double l_Distance = 0;

	std::cout << "Please provide a value for the distance: ";
	std::cin >> l_Distance;

	// If the data is invalid and does not match the type
	while(std::cin.fail()) {
		l_Distance = 0;
		std::cout << "\nInput was invalid. Please try again: ";
		std::cin.clear();
		std::cin.ignore(256, '\n');
		std::cin >> l_Distance;
	}

	return l_Distance;
}

double SystemClass::GetAccelerationInput(void)
{
	double l_Acceleration = 0;

	std::cout << "Please provide a value for the acceleration: ";
	std::cin >> l_Acceleration;

	// If the data is invalid and does not match the type
	while(std::cin.fail()) {
		l_Acceleration = 0;
		std::cout << "\nInput was invalid. Please try again: ";
		std::cin.clear();
		std::cin.ignore(256, '\n');
		std::cin >> l_Acceleration;
	}

	return l_Acceleration;
}

void SystemClass::Reset(void){
	// Reset Variables to restart process
	m_DistanceInput = 0;
	m_AccelerationInput = 0;
	m_DefuzzifictionResult = 0;

	return;
}

void SystemClass::CreateFuzzySets(void) {
	// Create and fill distance set
	m_DistanceSet = m_FuzzyHandler->CreateFuzzySet();
	m_DistanceSet->Initialise(7, -100, 100);
	m_DistanceSet->SetFuzzyValue(FUZZY_INFINITY, 0.25, FUZZY_INFINITY, 0.00001, "Far Left", 0);
	m_DistanceSet->SetFuzzyValue(0.125, 0.375, 0.25, 0.25, "Medium Left", 1);
	m_DistanceSet->SetFuzzyValue(0.325, 0.475, 0.4, 0.4, "Near Left", 2);
	m_DistanceSet->SetFuzzyValue(0.465, 0.535, 0.495, 0.505, "On Line", 3);
	m_DistanceSet->SetFuzzyValue(0.525, 0.675, 0.6, 0.6, "Near Right", 4);
	m_DistanceSet->SetFuzzyValue(0.625, 0.875, 0.75, 0.75, "Medium Right", 5);
	m_DistanceSet->SetFuzzyValue(0.75, FUZZY_INFINITY, 0.99999, FUZZY_INFINITY, "Far Right", 6);

	// Create and fill acceleration set
	m_AccelerationSet = m_FuzzyHandler->CreateFuzzySet();
	m_AccelerationSet->Initialise(7, -100, 100);
	m_AccelerationSet->SetFuzzyValue(FUZZY_INFINITY, 0.25, FUZZY_INFINITY, 0.00001, "Fast Left", 0);
	m_AccelerationSet->SetFuzzyValue(0.125, 0.375, 0.25, 0.25, "Normal Left", 1);
	m_AccelerationSet->SetFuzzyValue(0.325, 0.475, 0.4, 0.4, "Slow Left", 2);
	m_AccelerationSet->SetFuzzyValue(0.465, 0.535, 0.495, 0.505, "None", 3);
	m_AccelerationSet->SetFuzzyValue(0.525, 0.675, 0.6, 0.6, "Slow Right", 4);
	m_AccelerationSet->SetFuzzyValue(0.625, 0.875, 0.75, 0.75, "Normal Right", 5);
	m_AccelerationSet->SetFuzzyValue(0.75, FUZZY_INFINITY, 0.99999, FUZZY_INFINITY, "Fast Right", 6);

	// Create and fill defuzzification set
	m_DefuzzificationSet = m_FuzzyHandler->CreateFuzzySet();
	m_DefuzzificationSet->Initialise(7, -100, 100);
	m_DefuzzificationSet->SetFuzzyValue(FUZZY_INFINITY, 0.20, FUZZY_INFINITY, 0.00001, "Heavy Left", 0);
	m_DefuzzificationSet->SetFuzzyValue(0.15, 0.375, 0.24, 0.24, "Normal Left", 1);
	m_DefuzzificationSet->SetFuzzyValue(0.325, 0.475, 0.4, 0.4, "Light Left", 2);
	m_DefuzzificationSet->SetFuzzyValue(0.465, 0.535, 0.5, 0.5, "None", 3);
	m_DefuzzificationSet->SetFuzzyValue(0.525, 0.675, 0.6, 0.6, "Light Right", 4);
	m_DefuzzificationSet->SetFuzzyValue(0.625, 0.875, 0.76, 0.76, "Normal Right", 5);
	m_DefuzzificationSet->SetFuzzyValue(0.75, FUZZY_INFINITY, 0.99999, FUZZY_INFINITY, "Heavy Right", 6);

	// State end of function
	return;
}

void SystemClass::CreateFuzzyRules(void) 
{
	// Create a local store for the number of rules needed and set the value
	m_NumberOfFuzzyRules = 49;
	// Create a local store for the current rule being looked at
	int l_CurrentRule = 0;
	// Create an array of fuzzy rules
	m_FuzzyRules = new MYAI::FuzzyRule[m_NumberOfFuzzyRules];
	// Check if the fuzzy rule pointer created properly and return if it doesn't
	if(!m_FuzzyRules) {
		return;
	}

	// State end of function
	return;
}

// http://www.cplusplus.com/articles/4z18T05o/
void SystemClass::ClearScreen(void)
{
	HANDLE                     hStdOut;
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	DWORD                      count;
	DWORD                      cellCount;
	COORD                      homeCoords = { 0, 0 };

	hStdOut = GetStdHandle( STD_OUTPUT_HANDLE );
	if (hStdOut == INVALID_HANDLE_VALUE){
		return;
	}

	/* Get the number of cells in the current buffer */
	if (!GetConsoleScreenBufferInfo( hStdOut, &csbi )){
		return;
	}

	cellCount = csbi.dwSize.X *csbi.dwSize.Y;

	/* Fill the entire buffer with spaces */
	if (!FillConsoleOutputCharacter(hStdOut, (TCHAR) ' ', cellCount, homeCoords, &count)) {
		return;
	}

	/* Fill the entire buffer with the current colors and attributes */
	if (!FillConsoleOutputAttribute(hStdOut, csbi.wAttributes, cellCount, homeCoords, &count)){
		return;
	}

	/* Move the cursor home */
	SetConsoleCursorPosition( hStdOut, homeCoords );
	// State the end of the function
	return;
}

void SystemClass::AddToResults(int p_Number, double p_Addition)
{
	if(p_Number <= 2) {
		m_DefuzzifictionValues[0] = m_DefuzzifictionValues[0] + p_Addition;
	}
	if((p_Number == 3) || (p_Number == 4)) {
		m_DefuzzifictionValues[1] = m_DefuzzifictionValues[1] + p_Addition;
	}
	if(p_Number == 5) {
		m_DefuzzifictionValues[2] = m_DefuzzifictionValues[2] + p_Addition;
	}
	if(p_Number == 6) {
		m_DefuzzifictionValues[3] = m_DefuzzifictionValues[3] + p_Addition;
	}
	if(p_Number == 7) {
		m_DefuzzifictionValues[4] = m_DefuzzifictionValues[4] + p_Addition;
	}
	if((p_Number == 8) || (p_Number == 9)) {
		m_DefuzzifictionValues[5] = m_DefuzzifictionValues[5] + p_Addition;
	}
	if(p_Number >= 10) {
		m_DefuzzifictionValues[6] = m_DefuzzifictionValues[6] + p_Addition;
	}
	// End the function
	return;
}