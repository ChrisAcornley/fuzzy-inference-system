//////////////
// INCLUDES //
//////////////
#include "FuzzyControlClass.hpp"

////////////////
// NAMESPACES //
////////////////
using namespace MYAI;

////////////////////////////////////////////
// FUZZYCONTROLCLASS FUNCTION DEFINITIONS //
////////////////////////////////////////////
FuzzyControlClass::FuzzyControlClass(void)
{
	// Do nothing
}

FuzzyControlClass::~FuzzyControlClass(void)
{
	// Do nothing
}

FuzzySet* FuzzyControlClass::CreateFuzzySet()
{
	// Create a new Fuzzy Set and return the pointer to it 
	return new FuzzySet();
}

FuzzyRule* FuzzyControlClass::CreateFuzzyRule(int p_RuleOperation)
{
	// Create a new Fuzzy Rule
	FuzzyRule *l_TempRule;
	l_TempRule = new FuzzyRule();
	if(!l_TempRule) {
		return FUZZY_NULL;
	}
	
	// Check if the rule operation argument is above zero (therefore indicating the rule is an overload one)
	if(p_RuleOperation > 0) {
	// Pass in the rule operation
		l_TempRule->SetOperationLogic(p_RuleOperation);
	}
	
	// Return the fuzzy rule pointer
	return l_TempRule;
}

double* FuzzyControlClass::Run(FuzzySet* p_FuzzySet, double p_CrispInput)
{
	// Create a local store for the number of fuzzy values in the set
	int l_NumberOfFuzzyValues = p_FuzzySet->GetNumberOfFuzzyValues();

	// Create a local store for the fuzzy result that can hold the output for each fuzzy value
	double *l_Result;
	l_Result = new double[l_NumberOfFuzzyValues];

	// Check the fuzzy result array was successfully created and return NULL if it was not
	if(!l_Result) {
		return FUZZY_NULL;
	}

	// Check to see where the crisp input intersects with the fuzzy value
	// Go through each fuzzy value
	for(int i = 0; i < l_NumberOfFuzzyValues; i++) {
		// Get the result and put it into the array
		l_Result[i] = p_FuzzySet->IsCrispWithinRange(p_CrispInput, i);
	}

	// Return the result
	return l_Result;
}

double FuzzyControlClass::Defuzzification(double* p_RuleResults, int p_NumberOfRuleResults, FuzzySet* p_DefuzzificationSet)
{
	// Create a local defuzzification result
	double l_Result = 0;
	// Hold the current maxima value
	double l_CurrentMaxima = 0;
	// Local store for the number of maxima
	double l_NumberOfMaxima = 0;
	// Pointer for the current fuzzy value being reviewed
	FuzzyValue *l_CurrentFuzzyValue;

	// Each rule result is the membership of a fuzzy value in the defuzzification set
	// We can use this to map out the shape the set makes when the values are plugged in.
	/*

	|
	|
	|				 ______
	|				/	   \
	|			   /		\
	|		 _____/			 \
	|		/				  \
	|______/___________________\____________

	*/
	for(int i = 0; i < p_NumberOfRuleResults; i++) {
		// If the rule result is greater than zero we can proceed
		if(p_RuleResults[i] > 0) {
			// Add one to the number of maxima terms
			l_NumberOfMaxima += 1;
			// Using the mean of maximas rule to start with
			// Work out the line functions and plug in the rule result to get the x values. The difference between the
			// values multiplied by the height (rule results) will give an area. The mean can be found from that.
			l_CurrentFuzzyValue = p_DefuzzificationSet->GetFuzzyValue(i);
			double l_FirstLineChangeInX = l_CurrentFuzzyValue->GetFirstHighValue() - l_CurrentFuzzyValue->GetStartValue();
			double l_FirstLineGradiant = 1/l_FirstLineChangeInX;
			double l_FirstLineC = -(l_FirstLineGradiant*l_CurrentFuzzyValue->GetStartValue());

			double l_SecondLineChangeInX = l_CurrentFuzzyValue->GetEndValue() - l_CurrentFuzzyValue->GetSecondHighValue();
			double l_SecondLineGradiant = (-1)/l_SecondLineChangeInX;
			double l_SecondLineC = -(l_SecondLineGradiant*l_CurrentFuzzyValue->GetEndValue());

			// Normal functions is y = mx + c but we know y so mx = y-c and x = (y-c)/m

			double l_FirstXPosition = (p_RuleResults[i] - l_FirstLineC)/l_FirstLineGradiant;
			if(l_FirstLineChangeInX == 0) {
				l_FirstXPosition = 0;
			}
			double l_SecondXPosition = (p_RuleResults[i] - l_SecondLineC)/l_SecondLineGradiant;
			if(l_SecondLineChangeInX == 0) {
				l_SecondXPosition = 1;
			}

			l_CurrentMaxima += (p_DefuzzificationSet->GetActualFuzzyValue(l_FirstXPosition) + p_DefuzzificationSet->GetActualFuzzyValue(l_SecondXPosition))/2;
		}
	}
	// Get the centre of mass of the resulting object
	l_Result = l_CurrentMaxima/l_NumberOfMaxima;

	// Return the final defuzzification result
	return l_Result;
}