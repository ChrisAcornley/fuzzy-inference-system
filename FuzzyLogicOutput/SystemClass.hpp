#ifndef _SYSTEM_CLASS_HPP_
#define _SYSTEM_CLASS_HPP_

#include <iostream>
#include <conio.h>
#include <Windows.h>
#include "FuzzyControlClass.hpp"

class SystemClass
{
public:
	SystemClass(void);
	~SystemClass(void);
	void Initialise(void);
	void Run(void);
	int Shutdown(void);

private:
	double GetDistanceInput(void);
	double GetAccelerationInput(void);
	void Reset(void);
	void CreateFuzzySets(void);
	void CreateFuzzyRules(void);
	void ClearScreen(void);
	void AddToResults(int p_Number, double addition);

private:
	double m_DistanceInput;
	double m_AccelerationInput;
	double m_FuzzyOutput;
	double *m_DefuzzifictionValues;
	double m_DefuzzifictionResult;
	int m_NumberOfFuzzyRules;
	bool m_ProgramRunning;
	MYAI::FuzzyControlClass *m_FuzzyHandler;
	MYAI::FuzzySet *m_DistanceSet;
	MYAI::FuzzySet *m_AccelerationSet;
	MYAI::FuzzySet *m_DefuzzificationSet;
	MYAI::FuzzyRule *m_FuzzyRules;
};

#endif

