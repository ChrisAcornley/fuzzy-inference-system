// 13/02/2014
// This file contains any structs or enumerations that are needed by the 
// entirity of the AI.

/*

Fuzzy AI class will create fuzzy sets and fuzzy values
It will have to be able to compute the fuzzy output of a fuzzy set and be able
to perform defuzzification on the output

Pass in a fuzzy set with limits etc. and the input crisp value.
This must return an array which gives the membership values for the input.

Do this for both inputs

Pass these inputs through rules to see what the output is on each level or steering

Defuzzification must now be performed on the final answer

Will use a fairly general rule and use the center of mass

*/

#ifndef _AI_CONSTRUCTS_HPP_
#define _AI_CONSTRUCTS_HPP_

#include <string>

namespace MYAI {

	/////////////
	// DEFINES //
	/////////////
	#define FUZZY_INFINITY	0xFFFF
	#define FUZZY_NULL		0x00
	#define FUZZY_MULTIPLY	0x01
	#define FUZZY_DIVIDE	0x02
	#define FUZZY_ADD		0x03
	#define FUZZY_SUBTRACT	0x04
	#define FUZZY_OR		0x05
	#define FUZZY_AND		0x06
	#define FUZZY_NOR		0x07
	#define FUZZY_NAND		0x08

	///////////////////////
	// CLASS DEFINITIONS //
	///////////////////////

	// This class represents a fuzzy value. Each fuzzy value much contain five variables
	// The value where the fuzzy value starts	[1]
	// The value where the fuzzy value ends		[2]
	// The value of the first high point		[3]
	// The value of the second high point		[4]
	// The values passed to define where the membership function appears if it is modelled
	// on a graph are between zero to one as the fuzzy set contains the range the values 
	// the fuzzy values can sit in. There is an option to pass in FUZZY_INFINITY, which will 
	// anything that sits out of the defined range is encompassed by that fuzzy value. 
	// The sketch bellow helps explain the above text
	/*	
			 3________4							 3______________2 or 4
	|		 /		  \					|		 /
	|		/		   \				|		/
	|	   /			\				|	   /
	|	  /				 \				|	  /
	|	 /				  \				|	 /
	|	/				   \			|	/
	|_1/____________________\2____		|_1/____________________

	This shows what each value			Position two or four can be labelled infinity
	represents to the Fuzzy Class.		and this is the result.
										
	*/
	class FuzzyValue {
		//////////////////////
		// PUBLIC FUNCTIONS //
		//////////////////////
		public:
			// Constructor
			FuzzyValue();
			// Deconstructor
			~FuzzyValue();
			// Sets the start value
			void SetStart(double p_StartValue);
			// Sets the end value
			void SetEnd(double p_EndValue);
			// Sets the first high value
			void SetFirstHigh(double p_FirstHighValue);
			// Sets the second high value 
			void SetSecondHigh(double p_SecondHighValue);
			// Sets the name of the Fuzzy Value
			void SetValueName(std::string p_ValueName);
			// Returns the start value
			double GetStartValue();
			// Returns the end value
			double GetEndValue();
			// Returns the first high value
			double GetFirstHighValue();
			// Return the second high value
			double GetSecondHighValue();
			// Return the value name
			std::string GetValueName();

		///////////////////////
		// PRIVATE VARIABLES //
		///////////////////////
		private:
			double m_StartValue;		// Start Value
			double m_EndValue;			// End Value
			double m_FirstHighValue;	// First High Value
			double m_SecondHighValue;	// Second High Value
			std::string m_ValueName;	// The name of the fuzzy value
	}; //== End FuzzyValue ==//

	// This class is the Fuzzy Set Class. Each fuzzy set contains four variables.
	// The first is a pointer to a FuzzyValue class. This pointer is expanded into
	// an array dependent on the number passed into its initialise function.
	// The second is the number of Fuzzy Values in the set.
	// The third is the starting value of the fuzzy set range.
	// The fourth is the ending value of the fuzzy set range.
	// As the range of the set is contained here, the fuzzy values starting, ending,
	// first high and second high points are all between one and zero and are
	// translated into actual values by the fuzzy set.
	// The illistration below explains this.
	/*

		For example lets say the range of the set is 10 to 110 and that it only
		contains one fuzzy value as shown below with start, end, first high and
		second high values as 0.1, 0.3, 0.5 and 0.7 respectively.

			    0.3____0.5
		   |	  /	   \
		   |	 /		\
		   |	/		 \
		   |   /		  \
		   |  /			   \
		   |_/______________\_____
		0.0	0.1				0.7	  1.0

		The actual value of the fuzzy value when subject to the range would be
		the value of each point multiplied by the maximum range minus the minimum
		range value, then having the miniumum value added onto the result to give
		the final answer. 

			     40____60					
		   |	  /	   \					Max range value - 110
		   |	 /		\					Min range value - 10
		   |	/		 \					Max - Min (Range) = 110 - 10 = 100
		   |   /		  \					point multiped by Range (result) = 0.1*100 = 10
		   |  /			   \				result plus min value (answer) = 10 + 10 = 20
		   |_/______________\_____			answer = 20
		10	20				 80	  110		

	*/
	class FuzzySet {
		//////////////////////
		// PUBLIC FUNCTIONS //
		//////////////////////
		public:
			// Constructor
			FuzzySet();
			// Deconstructor
			~FuzzySet();
			// Initialise function to create set
			bool Initialise(int p_NumberOfFuzzyValues, double p_RangeStart, double p_RangeEnd);
			// Pass the values to a Fuzzy Value
			void SetFuzzyValue(double p_StartValue, double p_EndValue, double p_FirstHighValue, double p_SecondHighValue, std::string p_ValueName,int p_Number);
			// Returns a Fuzzy Value from the array
			FuzzyValue* GetFuzzyValue(int p_Number);
			// Get number of fuzzy values
			int GetNumberOfFuzzyValues();
			// Return the beginning value of the set range
			double GetSetRangeBeginning();
			// Return the end value of the set range
			double GetSetRangeEnd();
			// Checks if the crisp value is within the range of the fuzzy value
			double IsCrispWithinRange(const double p_CrispValue, const int p_Number);
			// Calculate the actual value from the fuzzy value
			double GetActualFuzzyValue(const double p_FuzzyValue);
			// Get a specific Fuzzy Values name
			std::string GetFuzzyValuesName(int p_Number);
			// Shutdown function - safely close points
			void Shutdown();

		///////////////////////
		// PRIVATE VARIABLES //
		///////////////////////
		private:
			// Get the membership value for the crisp input
			double GetCrispMembershipValue(const double p_CrispValue, const int p_Number);

		///////////////////////
		// PRIVATE VARIABLES //
		///////////////////////
		private:
			FuzzyValue *m_FuzzyValues;	// Pointer to a FuzzyValue Class - This will be expanded into an array to hold all the values
			int m_NumberOfFuzzyValues;	// Number of Fuzzy Values in the set
			double m_BeginRange;		// Starting value of the range
			double m_EndRange;			// End value of the range
	}; //== End FuzzySet ==//

	// This class represents a fuzzy rule in the AI system.
	// It acts by accepting four arguments in its creation and one to run
	// The first is the number of arguments the rule takes when combining inputs
	// The second is the number of logical operators it should use - this should always be one less than the number of
	// arguments. If not then the rule fails to create.
	// The third is an array of logical operators to use. It only has access to ID's in this array so the maths must be done
	// elsewhere.
	// The fourth is the logical opertor companions. Some of the operators are ones like multiply or add and these have to be
	// accompanied by a number. If not then it simply multiplies or divides by 0 and subtracts and adds 0
	class FuzzyRule {
		//////////////////////
		// PUBLIC FUNCTIONS //
		//////////////////////
		public:
			// Constructor
			FuzzyRule();
			// Constructor with argument
			FuzzyRule(int p_OperationID);
			// Deconstructor
			~FuzzyRule();
			// Runs the rule
			double RunRule(double* p_Arguments);
			// Set the rules logical operation
			void SetOperationLogic(int p_OperationID);

		///////////////////////
		// PRIVATE VARIABLES //
		///////////////////////
		private:
			// Perform AND operator
			double ANDOperation(double* p_Arguments);
			// Perform OR operator
			double OROperation(double* p_Arguments);
			// Perform NAND operator
			double NANDOperation(double* p_Arguments);
			// Perform NOR operator
			double NOROperation(double* p_Arguments);
			// Perform Multiply
			double MultiplyOperation(double* p_Arguments);
			// Perform Division
			double DivisionOperation(double* p_Arguments);
			// Perform Subtraction
			double SubtractionOperation(double* p_Arguments);
			// Perform Addition
			double AdditionOperation(double* p_Arguments);

		///////////////////////
		// PRIVATE VARIABLES //
		///////////////////////
		private:
			// Store for the rules operation
			int m_OperationID;

	}; //== End FuzzyRule ==//
} //== End myai ==//

#endif