// 19-Feb-2014

#include "SystemClass.hpp"

int main() {

	SystemClass *system;
	system = new SystemClass();

	if(!system) {
		return 0;
	}

	system->Initialise();

	system->Run();

	return system->Shutdown();
	// Display instructions
	// Ask for input
	// Pass input into the fuzzy inference system
	// Compute data
	// Display output
	// await next set of inputs
}