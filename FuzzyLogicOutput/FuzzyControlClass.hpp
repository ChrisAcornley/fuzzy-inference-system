// 13/02/2014

#ifndef _FUZZY_CONTROL_CLASS_HPP_
#define _FUZZY_CONTROL_CLASS_HPP_

#include "AIConstructs.hpp"

namespace MYAI {

	class FuzzyControlClass
	{
		//////////////////////
		// PUBLIC FUNCTIONS //
		//////////////////////
		public:
			// Constructor
			FuzzyControlClass(void);
			// Deconstructor
			~FuzzyControlClass(void);
			// Creates a Fuzzy Set
			FuzzySet* CreateFuzzySet();
			// Create a Fuzzy Rule
			FuzzyRule* CreateFuzzyRule(int p_RuleOperation);
			// Run Function - Pass in a value and a fuzzy set
			double* Run(FuzzySet* p_FuzzySet, double p_CrispInput);
			// Defuzzification function
			double Defuzzification(double* p_RuleResults, int p_NumberOfRuleResults, FuzzySet* p_DefuzzificationSet);
	};
}
#endif

